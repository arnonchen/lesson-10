#include "Item.h"


Item::Item(std::string name, std::string sNumber, double price)
{
	_name = name;
	_serialNumber = sNumber;
	_unitPrice = price;
	_count = 1;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return( _count * _unitPrice);
}

bool Item::operator <(const Item& other) const
{
	if (other.getSerial() > this->_serialNumber)
	{
		return true;
	}
	return false;
}

bool Item::operator >(const Item& other) const
{
	if (other.getSerial() > this->_serialNumber)
	{
		return false;
	}
	return true;
}

bool Item::operator ==(const Item& other) const
{
	if (other.getSerial() == this->_serialNumber)
	{
		return true;
	}
	return false;
}

std::string Item::getName() const
{
	return _name;
}

std::string Item::getSerial() const
{
	return _serialNumber;
}

int Item::getCount() const
{
	return _count;
}

double Item::getUnitPrice() const
{
	return _unitPrice;
}


void Item::setName(std::string name)
{
	_name = name;
}
void Item::setSerial(std::string serial)
{
	_serialNumber = serial;
}
void Item::setCount(int count)
{
	_count = count;
}
void Item::setUnitPrice(double price)
{
	_unitPrice = price;
}