#include "Customer.h"

Customer::Customer(std::string name)
{
	_name = name;
}

Customer::Customer()
{

}

double Customer::totalSum() const
{
	double sum = 0;
	std::set<Item>::iterator it;
	for (it = _items.begin(); it != _items.end(); ++it)
	{
		sum += (*it).totalPrice();
	}
	return sum;
}


void Customer::addItem(Item a)
{
	std::set<Item>::iterator it = _items.find(a);
	if (it != _items.end())
	{
		Item temp = *it;
		temp.setCount(it->getCount() + 1);
		_items.erase(it);
		_items.insert(temp);
	}
	else
		_items.insert(a);
}

void Customer::removeItem(Item a)
{
	std::set<Item>::iterator it = _items.find(a);
	if (it != _items.end())
	{
		Item temp = *it;
		if (it->getCount() != 1)
		{
			temp.setCount(it->getCount() - 1);
			_items.erase(it);
			_items.insert(temp);
		}
		else
			_items.erase(it);
	}
}

std::string Customer::getName() const
{
	return _name;
}

std::set<Item> Customer::getItems() const
{
	return _items;
}