#include"Customer.h"
#include<map>
#include <iostream>
void itemPrinter(Item* items);
int main()
{
	bool flag = false;
	std::string name;
	int choice = 0;
	std::map<std::string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};
	while (choice != 4)
	{
		flag = false;
		std::cout << "Welcome to MagshiMart!\n1. to sign as customer and buy items\n2. to uptade existing customer's items\n3. to print the customer who pays the most\n4. to exit\n";
		std::cin >> choice;
		if (choice == 1)
		{
			std::cout << "enter your name:\n";
			std::cin >> name;
			if (!(abcCustomers.count(name)))
			{
				int itemNum = 1;
				Customer c = Customer(name);
				itemPrinter(itemList);
				while (itemNum != 0)
				{
					std::cin >> itemNum;
					if (itemNum != 0)
					{
						if (itemNum > 10)
						{
							std::cout << "no such item exists\n";
						}
						else
						{
							c.addItem(itemList[itemNum - 1]);
						}
					}
				}
				abcCustomers.insert({ name, c });
			}
			else
				std::cout << "customer already exists\n";
		}
		else if (choice == 2)
		{
			std::cout << "enter your name:\n";
			std::cin >> name;
			if (abcCustomers.count(name))
			{
				std::map<std::string, Customer>::iterator it;
				it = abcCustomers.find(name);
				Customer c = it->second;
				std::cout << "1. Add items\n2. Remove items\n3. Back to menu\n";
				int innerChoice;
				std::cin >> innerChoice;
				if (innerChoice == 1)
				{
					int itemNum = 1;
					Customer c = Customer(name);
					itemPrinter(itemList);
					while (itemNum != 0)
					{
						std::cin >> itemNum;
						if (itemNum != 0)
						{
							if (itemNum > 10)
							{
								std::cout << "no such item exists\n";
							}
							else
							{
								c.addItem(itemList[itemNum - 1]);
							}
						}
						abcCustomers.erase(name);
						abcCustomers.insert({ name, c });
					}
					abcCustomers.insert({ name, c });
				}
				else if (innerChoice == 2)
				{
					choice = 1;
					while (choice != 0)
					{
						std::cout << "enter item to be terminated\n";
						std::cin >> choice;
						if (choice != 0)
						{
							c.removeItem(itemList[choice - 1]);
						}
					}
					abcCustomers.erase(name);
					abcCustomers.insert({ name, c });
				}
				else if (innerChoice)
				{
					break;
				}
				else
				{
					std::cout << "wait, thats illegal";
				}
			}
			else
				std::cout << "customer dosnt exist";
		}
		else if (choice == 3)
		{
			if (!(abcCustomers.empty()))
			{
				std::map<std::string, Customer>::iterator it;
				Customer maxCustomerMan = abcCustomers.begin()->second;
				for (it = abcCustomers.begin(); it != abcCustomers.end(); it++)
				{
					if (it->second.totalSum() > maxCustomerMan.totalSum())
					{
						maxCustomerMan = it->second;
					}
				}
				std::cout << maxCustomerMan.getName() << " and he paid " << maxCustomerMan.totalSum() << "\n";
			}
		}
		else if(choice == 4)
		{
			std::cout << "bye\n";
		}
		else
		{
			std::cout << "illegal choice\n";
		}
	}
	return 0;
}

void itemPrinter(Item* items)
{
	std::cout << "The items you can buy are: (0 to exit)\n";
	for (int i = 0; i < 10; i++)
	{
		std::cout << items[i].getName() << ", " << items[i].getSerial() << ", " << items[i].getUnitPrice() << "\n";
	}
}